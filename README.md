# Pixelize URP Shader

A pretty simple outline shader for URP, using a RenderFeature  
Was made for using together with custom pixelization shader  
https://gitlab.com/jojo-master-public/pixelize-shader

## Features

* Crisp 1px outline.

## Getting Started

* Add Universal Render Pipeline package into your project.  
* Add Pixelization Render Feature from above  
* Add contents of this repo anywhere in the project.  
* Add Outline render feature into your URP Asset  
* Add OutlineComponent post process into your Post-Processing stack  
* Set Size to the same value as PixelizeComponent
* Enjoy!  

## Preview

![no-outline](previews/no-pixelization.png)  
![with-outline](previews/with-pixelization.png)  

## Contact

Developed by Andrew Dioinecail.  
Twitter: [Dioinecail](https://twitter.com/dioinecail).  
Email: andrewdionecail@gmail.com  
  
Feel free to ask questions!  